<?php
/**
 *  Info
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    07.12.2021
 * Time:    13:29
 */
namespace IK\YooKassa\Block;
use IK\YooKassa\Gateway\Response\PaymentLinkHandler;
use Magento\Framework\Phrase;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\View\Element\Template as FrontendTemplate;
use Magento\Framework\View\Element\Template\Context;
use Magento\Payment\Block\ConfigurableInfo;
use Magento\Payment\Gateway\ConfigInterface;

/**
 *
 */
class Info extends ConfigurableInfo {
    /**
     * @var SerializerInterface
     */
    protected $_serializer;

    /**
     * @param SerializerInterface $serializer
     * @param Context             $context
     * @param ConfigInterface     $config
     * @param array               $data
     */
    public function __construct(
        SerializerInterface $serializer,
        Context $context,
        ConfigInterface $config,
        array $data = []
    ) {
        parent::__construct($context, $config, $data);
        $this->_serializer = $serializer;
    }

    /**
     * Returns label
     *
     * @param string $field
     * @return Phrase
     */
    protected function getLabel($field) {
        return __($field);
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function _toHtml() {
        if($this->getInfo()->getAdditionalData()){
            $additionalData = $this->_serializer->unserialize($this->getInfo()->getAdditionalData());

            if(isset($additionalData[PaymentLinkHandler::PAYMENT_ADDITIONAL_DATA_REDIRECT_URL_CODE])){
                $blockAdditionalData = $this->getLayout()->getBlock('yookassa.payments.linkToAuthorize');
                if(!$blockAdditionalData){
                    $blockAdditionalData = $this->getLayout()->createBlock(FrontendTemplate::class,'yookassa.payments.linkToAuthorize');
                    $blockAdditionalData->setTemplate('IK_YooKassa::info/default/payment_link.phtml');
                    $this->setChild('linkToAuthorize', $blockAdditionalData);
                }
                $blockAdditionalData->setPaymentLink($additionalData[PaymentLinkHandler::PAYMENT_ADDITIONAL_DATA_REDIRECT_URL_CODE]);
            }
        }
        return parent::_toHtml();
    }
}
