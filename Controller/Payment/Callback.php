<?php
/**
 *  Callback
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    11.12.2021
 * Time:    17:39
 */
namespace IK\YooKassa\Controller\Payment;
use IK\YooKassa\Gateway\Response\Initialize\OrderState;
use IK\YooKassa\Helper\Data;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\DataObject;
use Magento\Payment\Model\Method\Logger;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;
use Magento\Sales\Model\Order\Invoice\NotifierInterface as InvoiceNotifierInterface;
use Magento\Sales\Model\Order\Payment;
use Magento\Sales\Model\Order\Payment\Operations\AuthorizeOperation;
use Magento\Sales\Model\Order\Payment\Operations\SaleOperation;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\OrderRepository;
use YooKassa\Model\Notification\NotificationCanceled;
use YooKassa\Model\Notification\NotificationFactory;
use YooKassa\Model\Notification\NotificationSucceeded;
use YooKassa\Model\Notification\NotificationWaitingForCapture;
use YooKassa\Request\Payments\CreatePaymentResponse;

/**
 *
 */
class Callback implements HttpPostActionInterface, CsrfAwareActionInterface {
    /**
     * @var Data
     */
    protected $_helper;
    /**
     * @var Logger
     */
    protected $_logger;
    /**
     * @var RequestInterface
     */
    protected $_request;
    /**
     * @var Order|null
     */
    protected $_order = null;
    /**
     * @var OrderFactory
     */
    protected $_orderFactory;
    /**
     * @var ResultFactory
     */
    protected $_resultFactory;
    /**
     * @var OrderRepository
     */
    protected $_orderRepository;
    /**
     * @var OrderSender
     */
    protected $_orderSender;

    /**
     * @var ResponseInterface
     */
    protected $_response;
    /**
     * @var InvoiceNotifierInterface
     */
    protected $_notifierInvoice;
    /**
     * @var SaleOperation
     */
    protected $_saleOperation;
    /**
     * @var AuthorizeOperation
     */
    protected $_authorizeOperation;


    /**
     * @param Data                     $helper
     * @param Logger                   $logger
     * @param OrderRepository          $orderRepository
     * @param OrderFactory             $orderFactory
     * @param OrderSender              $orderSender
     * @param AuthorizeOperation       $authorizeOperation
     * @param SaleOperation            $saleOperation
     * @param InvoiceNotifierInterface $invoiceNotifier
     * @param ResultFactory            $resultFactory
     * @param RequestInterface         $request
     * @param ResponseInterface        $response
     */
    public function __construct(
        Data                              $helper,
        Logger                            $logger,
        OrderRepository                   $orderRepository,
        OrderFactory                      $orderFactory,
        OrderSender                       $orderSender,
        AuthorizeOperation                $authorizeOperation,
        SaleOperation                     $saleOperation,
        InvoiceNotifierInterface          $invoiceNotifier,
        ResultFactory                     $resultFactory,
        RequestInterface                  $request,
        ResponseInterface                 $response
    ){
        $this->_helper   = $helper;
        $this->_logger   = $logger;
        $this->_request  = $request;
        $this->_response = $response;
        $this->_orderFactory    = $orderFactory;
        $this->_resultFactory   = $resultFactory;
        $this->_orderRepository = $orderRepository;
        $this->_orderSender     = $orderSender;
        $this->_notifierInvoice = $invoiceNotifier;
        $this->_saleOperation   = $saleOperation;
        $this->_authorizeOperation = $authorizeOperation;
    }


    /**
	 * @inheritDoc
	 */
	public function execute() {
        $content = $this->_request->getContent();
        /** here we use php function json_decode!!! */
        $responseArray  = json_decode($content,true);
        $this->_logger->debug(['callback' => 'Run YooKassa callback controller']);
        $this->_logger->debug($responseArray);

        try {
            $notificationResponse = new NotificationFactory;
            $notification = $notificationResponse->factory($responseArray);
        } catch (\Exception $e){
            $this->_logger->debug(['Callback Notofication Object Error' => $e->getMessage()]);
            return $this->_resultFactory->create(ResultFactory::TYPE_RAW);
        }

        if($notification instanceof NotificationCanceled || $notification instanceof NotificationSucceeded){
            /** We do sleep to avoid double run of order cancellation */
            sleep(8);
        }


        $this->_loadOrder($responseArray);
        if(!$this->_order->getId()){
            return $this->_resultFactory->create(ResultFactory::TYPE_RAW);
        }
        /** @var Payment $payment */
        $payment = $this->_order->getPayment();
        $storeId = $this->_order->getStoreId();

        /** @var CreatePaymentResponse $responsePayment */
        $responsePayment = $notification->getObject();

        if(($notification instanceof NotificationCanceled) && !$this->_order->isCanceled()){

            $this->_logger->debug(['Callback Step' => 'response with status canceled']);
            if ($this->_order->canCancel()) {
                $this->_order->cancel();
                $this->_order->addCommentToStatusHistory(__('Payment was canceled from YooKassa side.'));
            } else {
                $this->_order->addCommentToStatusHistory(__('Attempt cancel payment from YooKassa side.'));
            }
            $this->_logger->debug(['Callback Step' => 'after order canceled']);

            $this->_orderRepository->save($this->_order);
            return $this->_resultFactory->create(ResultFactory::TYPE_RAW);
        }

        /** check if order not canceled */
        if($this->_order->isCanceled() && !$notification instanceof NotificationCanceled) {
            /** todo: сделать ручной запрос на отмену если статус waiting for capture или refund если платёж уже был оплечен. */
            $this->_order->addCommentToStatusHistory($this->_prepareComment($responsePayment->getId(),__('Cannot process payment. Order is canceled.')));
            try {
                //$payment->void(new DataObject());
            } catch (\Exception $exception ){
                $this->_logger->debug(['Callback Exception' => $exception->getMessage()]);
            }
            $this->_orderRepository->save($this->_order);
            return $this->_resultFactory->create(ResultFactory::TYPE_RAW);
        }


        $this->_logger->debug(['Callback Step' => 'order is needed state']);
        try {
            $totalDue = $this->_order->getTotalDue();
            $baseTotalDue = $this->_order->getBaseTotalDue();
            /** Check if order is in a state after initialization */
            if($notification instanceof NotificationWaitingForCapture
                && (in_array($this->_order->getState(), [OrderState::INITIALIZED_PAYMENT_ORDER_STATE_VALUE]))
                ) {
                //$this->_logger->debug(['Callback Step' => 'response NotificationWaitingForCapture']);
                $this->_authorizeOperation->authorize($payment,true,$baseTotalDue);
                //$this->_logger->debug(['Callback Step' => 'after authorize']);
                // base amount will be set inside
                $payment->setAmountAuthorized($totalDue);
            }

            /** process case when payment is captured on YooKassa side. autocapture or manually */
            if($notification instanceof NotificationSucceeded
                && in_array($this->_order->getState(),[OrderState::INITIALIZED_PAYMENT_ORDER_STATE_VALUE,Order::STATE_PROCESSING,Order::STATE_HOLDED,Order::STATE_PAYMENT_REVIEW])) {
                //$this->_logger->debug(['Callback Step' => 'response NotificationSucceeded']);
                if($this->_order->canInvoice()){
                    //$this->_logger->debug(['Callback Step' => 'can invoice']);
                    $payment->setAmountAuthorized($totalDue);
                    $payment->setBaseAmountAuthorized($baseTotalDue);
                    $this->_saleOperation->execute($payment);
                    //$this->_logger->debug(['Callback Step' => 'after sale operation']);
                } else {
                    $this->_order->addCommentToStatusHistory($this->_prepareComment($responsePayment->getId(),__('Order has been already invoiced.')));
                }

                if($this->_helper->ifSendInvoiceEmail($storeId)){
                    //$this->_logger->debug(['Callback Step' => 'after send invoice']);
                    /** @var Order\Invoice $invoice */
                    $invoice = $payment->getCreatedInvoice();
                    $this->_notifierInvoice->notify($this->_order, $invoice);
                    //$this->_logger->debug(['Callback Step' => 'after send invoice notification']);
                }
            }

            $this->_logger->debug(['Callback Step' => 'before send email notification']);
            if (!$this->_order->getEmailSent()) {
                $this->_orderSender->send($this->_order);
                $this->_order->addCommentToStatusHistory( __('The order confirmation email was sent'));
            }
        } catch (\Exception $e ){
            $this->_order->addCommentToStatusHistory($e->getMessage());
            $this->_logger->debug(['Callback Exception' => $e->getMessage()]);
        }

        $this->_orderRepository->save($this->_order);
        return $this->_resultFactory->create(ResultFactory::TYPE_RAW);
	}

    /**
     * @inheritDoc
     */
    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException {
        $this->_logger->debug([sprintf("Rejected request %s :", $request->getActionName()) => $request->getParams()]);
        return null;
    }

    /**
     * @inheritDoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool {
        return true;
    }

    /**
     * @param array $response
     *
     * @return void
     */
    protected function _loadOrder(array $response){

        if(!isset($response['object']['metadata']['order_id']) || !isset($response['object']['id'])){
            $this->_logger->debug(['Exception' => 'no id or order id']);
            return;
        }

        $orderIncrementId = $response['object']['metadata']['order_id'];
        $this->_order = $this->_orderFactory->create()->loadByIncrementId($orderIncrementId);
    }

    /**
     * @param string $paymentId
     * @param string $message
     *
     * @return string
     */
    protected function _prepareComment($paymentId, string $message){
        return sprintf(__("%s Transaction ID: \"%s\""),$message, $paymentId);
    }
}
