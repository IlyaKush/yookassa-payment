<?php
/**
 *  Returns
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    10.12.2021
 * Time:    21:31
 */
namespace IK\YooKassa\Controller\Payment;
use IK\YooKassa\Gateway\Request\Initialization;
use IK\YooKassa\Helper\Data;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderFactory;
/**
 *
 */
class Returns implements HttpGetActionInterface {
//class Returns implements ActionInterface {
    /**
     * @var CheckoutSession
     */
    protected $_checkoutSession;
    /**
     * @var MessageManagerInterface
     */
    protected $_messageManager;
    /**
     * @var ResultFactory
     */
    protected $_resultFactory;
    /**
     * @var RequestInterface
     */
    protected $_request;
    /**
     * @var OrderFactory
     */
    protected $_orderFactory;
    /**
     * @var Data
     */
    protected $_helper;


    /**
     * @param RequestInterface        $request
     * @param ResultFactory           $resultFactory
     * @param MessageManagerInterface $messageManager
     * @param CheckoutSession         $session
     * @param OrderFactory            $orderFactory
     * @param Data                    $helper
     */
    public function __construct(
        RequestInterface        $request,
        ResultFactory           $resultFactory,
        MessageManagerInterface $messageManager,
        CheckoutSession         $session,
        OrderFactory            $orderFactory,
        Data                    $helper
    ) {
        $this->_checkoutSession = $session;
        $this->_messageManager  = $messageManager;
        $this->_resultFactory   = $resultFactory;
        $this->_request         = $request;
        $this->_orderFactory    = $orderFactory;
        $this->_helper = $helper;
    }
	/**
	 * @inheritDoc
	 */
	public function execute() {

        if($this->_request->getParam(Initialization::RETURNS_CONTROLLER_PARAM_CODE_ORDER)){
            $orderIncrementId = $this->_request->getParam(Initialization::RETURNS_CONTROLLER_PARAM_CODE_ORDER);
        } else {
            $orderIncrementId = $this->_checkoutSession->getLastRealOrderId();
        }

        if(!$orderIncrementId){
            return $this->_redirect('');
        }

        /** @var Order $order */
        $order = $this->_orderFactory->create()->loadByIncrementId($orderIncrementId);

        if(!$order->getId()){
            return $this->_redirect('');
        }

        if($order->isCanceled()){
            $this->_checkoutSession->restoreQuote();
            return $this->_redirectToCheckout($order->getStoreId());
        } else {
            if(($orderIncrementId == $this->_checkoutSession->getLastRealOrderId())){
                return $this->_redirect('checkout/onepage/success');
            } else {

                if($order->getEmailSent()){
                    $this->_messageManager->addSuccessMessage(__('Order %1 has been processing.',$orderIncrementId));
                    return $this->_redirect('');
                } else {
                    $this->_messageManager->addSuccessMessage(__("We'll email you an order confirmation with details and tracking info."));
                    return $this->_redirect('checkout/cart');
                }
            }
        }
	}

    /**
     * @param string $path
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    protected function _redirect(string $path, $params = []){
        return $this->_resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath($path,$params);
    }

    /**
     * @param null|int|string $storeId
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    protected function _redirectToCheckout($storeId = null){
        $params = ['_fragment' => 'payment'];
        if($this->_helper->isOneStepCheckout($storeId)){
            $params = [];
        }
        return $this->_redirect('checkout',$params);
    }
}
