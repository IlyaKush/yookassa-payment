<?php
/**
 *  General
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    07.12.2021
 * Time:    13:43
 */
namespace IK\YooKassa\Gateway\Config;
use IK\YooKassa\Helper\Data;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Payment\Gateway\ConfigInterface;
use Magento\Store\Model\ScopeInterface;
/**
 *
 */
class General implements ConfigInterface {

    const DEFAULT_PATH_PATTERN            = 'payment/%s/%s';

    /** Set of general settings*/
    /** todo: review it with last set of settings */
    const IN_COMMON_FIELDS                = [
        Data::SHOP_ID_XML_CODE,
        Data::PRIVATE_KEY_XML_CODE,
        Data::TESTMODE_XML_CODE,
        Data::SEND_ORDER_EMAIL_XML_CODE,
        Data::SEND_INVOICE_EMAIL_XML_CODE,
        Data::AUTOCAPTURE_XML_CODE,
        Data::YOO_PAYMENT_MODE_XML_CODE,
        Data::YOO_PAYMENT_SUBJECT_XML_CODE,
        'debug',
        'debugReplaceKeys',
        'privateInfoKeys',
        'paymentInfoKeys'
    ];

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var string|null
     */
    protected $methodCode;

    /**
     * @var string|null
     */
    protected $pathPattern;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param string|null          $methodCode
     * @param string               $pathPattern
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
                             $methodCode = null,
                             $pathPattern = self::DEFAULT_PATH_PATTERN
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->methodCode   = $methodCode;
        $this->pathPattern = $pathPattern;
    }

	/**
	 * @inheritDoc
	 */
	public function getValue($field, $storeId = null) {
        if(in_array($field,self::IN_COMMON_FIELDS)){
            $methodCode = DATA::GENERAL_SETTINGS_CODE;
        } else {
            $methodCode = $this->methodCode;
        }

        if ($methodCode === null || $this->pathPattern === null) {
            return null;
        }

        return $this->_scopeConfig->getValue(
            sprintf($this->pathPattern, $methodCode, $field),
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
	}

	/**
	 * @inheritDoc
	 */
	public function setMethodCode($methodCode) {
        $this->methodCode = $methodCode;
	}

	/**
	 * @inheritDoc
	 */
	public function setPathPattern($pathPattern) {
        $this->pathPattern = $pathPattern;
	}

    /**
     * @param string   $path
     * @param int|null $storeId
     *
     * @return mixed
     */
    public function getConfigValue($path, $storeId = null)  {
        return $this->_scopeConfig->getValue($path, ScopeInterface::SCOPE_STORE,$storeId);
    }
}
