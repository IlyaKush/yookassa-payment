<?php
/**
 *  VatCode
 *
 * @copyright Copyright © 2022 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    22.01.2022
 * Time:    21:46
 */
namespace IK\YooKassa\Gateway\Helper;

/**
 * @see https://yookassa.ru/developers/54fz/parameters-values#vat-codes
 */
class VatCodeProcessor {

    /**
     * @param float $taxValue
     *
     * @return int
     */
    public function getVatCode($taxValue){
        switch($taxValue){
            case 0: return 2; //НДС по ставке 0%
            case 10: return 3; //НДС по ставке 10%
            case 20: return 4; //НДС по ставке 20%
        }

        if(($taxValue >= 9) && ($taxValue <= 9.1)){
            return 5; //НДС чека по расчетной ставке 10/110
        }

        if(($taxValue >= 16) && ($taxValue <= 16.7)){
            return 6; //НДС чека по расчетной ставке 20/120
        }
        return 1;
    }
}
