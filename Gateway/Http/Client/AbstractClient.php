<?php
/**
 *  AbstractClient
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    08.12.2021
 * Time:    21:06
 */
namespace IK\YooKassa\Gateway\Http\Client;
use IK\YooKassa\Helper\Data;
use Magento\Payment\Gateway\Http\ClientException;
use Magento\Payment\Gateway\Http\ClientInterface;
use Magento\Payment\Gateway\Http\TransferInterface;
use Magento\Payment\Model\Method\Logger;
use YooKassa\Client;
use YooKassa\Request\Payments\PaymentResponse;
use YooKassa\Request\Refunds\RefundResponse;

/**
 *
 */
abstract class AbstractClient implements ClientInterface {

    /**
     * @var Data
     */
    protected $_helper;
    /**
     * @var Logger
     */
    protected Logger $_logger;

    /**
     * @param Data   $helper
     * @param Logger $logger
     */
    public function __construct(
        Data            $helper,
        Logger          $logger
    ) {
        $this->_logger = $logger;
        $this->_helper = $helper;
    }

	/**
	 * @inheritDoc
	 */
	public function placeRequest(TransferInterface $transferObject) {
        $parameters = $transferObject->getBody();
        $response   = [];
        try {
            $clientResponse = $this->_doRequest($parameters);
            $message = sprintf("Success");
            $response = $clientResponse->toArray();
        } catch (\Exception $e) {
            $message = __($e->getMessage() ?: 'Sorry, but something went wrong');
            throw new ClientException($message);
        } finally{
            $this->_logger->debug(
                [
                    'class'    => get_class($this),
                    'message'  => $message,
                    'request'  => $transferObject->getBody(),
                    'response' => $response
                ]
            );
        }

        return $response;
	}

    /**
     * @param array $parameters
     *
     * @return PaymentResponse|RefundResponse|null
     */
    abstract protected function _doRequest($parameters);

    /**
     * @param $storeId
     *
     * @return Client
     */
    protected function _getGatewayClient($storeId = null) {
        $yooKasssShopId = $this->_helper->getShopId($storeId);
        $privateKey = $this->_helper->getPrivateKey($storeId);

        $client = new Client();
        $client->setAuth($yooKasssShopId, $privateKey);

        return $client;
    }

    /**
     * @param array $parameters
     *
     * @return array
     */
    protected function _cleanUpPrarametrs($parameters){
        unset($parameters['id'],$parameters['store_id'],$parameters['idempotence_key'],$parameters['txn_type']);
        return $parameters;
    }

}
