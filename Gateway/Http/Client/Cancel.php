<?php
/**
 *  Cancel
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    13.12.2021
 * Time:    21:54
 */
namespace IK\YooKassa\Gateway\Http\Client;
/**
 *
 */
class Cancel extends AbstractClient {

	/**
	 * @inheritDoc
	 */
	protected function _doRequest($parameters) {
        $paymentId = $parameters['id'];
        $storeId   = $parameters['store_id'];
        $idempotenceKey = $parameters['idempotence_key'];
        $client = $this->_getGatewayClient($storeId);
        return $client->cancelPayment($paymentId,$idempotenceKey);
	}
}
