<?php
/**
 *  Capture
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    12.12.2021
 * Time:    22:10
 */
namespace IK\YooKassa\Gateway\Http\Client;
use YooKassa\Request\Payments\CreatePaymentResponse;
/**
 *
 */
class Capture extends AbstractClient {
	/**
	 * @inheritDoc
	 */
	protected function _doRequest($parameters) {
        $paymentId = $parameters['id'];
        $storeId   = $parameters['store_id'];
        $idempotenceKey = $parameters['idempotence_key'];
        $client = $this->_getGatewayClient($storeId);
        return $client->capturePayment( $this->_cleanUpPrarametrs($parameters),$paymentId,$idempotenceKey);
	}
}
