<?php
/**
 *  Get
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    11.12.2021
 * Time:    22:23
 */
namespace IK\YooKassa\Gateway\Http\Client;
use Magento\Sales\Api\Data\TransactionInterface;

/**
 *
 */
class Get extends AbstractClient {

	/**
	 * @inheritDoc
	 */
	protected function _doRequest($parameters) {
        $paymentId = $parameters['id'];
        $storeId   = $parameters['store_id'];
        $client = $this->_getGatewayClient($storeId);
        if(isset($parameters['txn_type'])){
            if($parameters['txn_type'] == TransactionInterface::TYPE_REFUND){
                return $client->getRefundInfo($paymentId);
            }
        }
        return $client->getPaymentInfo($paymentId);
	}
}
