<?php
/**
 *  Initialization
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    10.12.2021
 * Time:    13:21
 */
namespace IK\YooKassa\Gateway\Http\Client;
use YooKassa\Request\Payments\CreatePaymentResponse;
/**
 *
 */
class Initialization extends AbstractClient {

	/**
	 * @inheritDoc
	 */
	protected function _doRequest($parameters) {
        $storeId = $parameters['store_id'];
        $idempotenceKey = $parameters['idempotence_key'];
        $client = $this->_getGatewayClient($storeId);
        $response = $client->createPayment($this->_cleanUpPrarametrs($parameters),$idempotenceKey);
        return $response;
	}
}
