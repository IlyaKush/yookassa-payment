<?php
/**
 *  Refund
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    15.12.2021
 * Time:    17:11
 */
namespace IK\YooKassa\Gateway\Http\Client;
/**
 *
 */
class Refund extends AbstractClient {

	/**
	 * @inheritDoc
	 */
	protected function _doRequest($parameters) {
        $storeId   = $parameters['store_id'];
        $idempotenceKey = $parameters['idempotence_key'];
        $parameters['payment_id'] = $parameters['id'];
        $client = $this->_getGatewayClient($storeId);
        return $client->createRefund($this->_cleanUpPrarametrs($parameters),$idempotenceKey);
	}
}
