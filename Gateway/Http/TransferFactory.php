<?php
/**
 *  TransferFactory
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    08.12.2021
 * Time:    22:04
 */
namespace IK\YooKassa\Gateway\Http;
use Magento\Payment\Gateway\Http\TransferBuilder;
use Magento\Payment\Gateway\Http\TransferFactoryInterface;
/**
 *
 */
class TransferFactory implements TransferFactoryInterface {
    /**
     * @var TransferBuilder
     */
    protected $transferBuilder;

    /**
     * @param TransferBuilder $transferBuilder
     */
    public function __construct(
        TransferBuilder $transferBuilder
    ) {
        $this->transferBuilder = $transferBuilder;
    }

	/**
	 * @inheritDoc
	 */
	public function create(array $request) {
        return $this->transferBuilder
            ->setBody($request)
            ->build();
	}
}
