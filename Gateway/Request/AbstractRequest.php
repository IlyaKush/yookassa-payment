<?php
/**
 *  AbstractRequest
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    08.12.2021
 * Time:    23:08
 */
namespace IK\YooKassa\Gateway\Request;
use IK\YooKassa\Gateway\Response\GatewayTransId;
use IK\YooKassa\Helper\Data;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Payment\Model\Method\Logger;
use Magento\Sales\Model\Order\Payment as OrderPayment;

/**
 * @see https://yookassa.ru/developers/api
 */
abstract class AbstractRequest implements BuilderInterface {
    /**
     * @var Data
     */
    protected $_helper;
    /**
     * @var Logger
     */
    protected $_logger;
    /**
     * @var SerializerInterface
     */
    protected $_serializer;

    /**
     * @param SerializerInterface $serializer
     * @param Data                $helper
     * @param Logger              $logger
     */
    public function __construct(
        SerializerInterface $serializer,
        Data                $helper,
        Logger              $logger
    ) {
        $this->_helper = $helper;
        $this->_logger = $logger;
        $this->_serializer = $serializer;
    }

    /**
     * @param string $key
     *
     * @return string
     */
    protected function _generateIdempotenceKey($key = 'ik.yookassa'){
        $idempotenceKey = uniqid(sprintf("%s.", $key), true);
        return $idempotenceKey;
    }

    /**
     * @param OrderPayment $payment
     *
     * @return string
     */
    protected function _getGatewayPaymentId($payment){
        $additional = $payment->getAdditionalData();
        if($additional) {
            $additional = $this->_serializer->unserialize($additional);
            $paymentId  = $additional[GatewayTransId::PAYMENT_ADDITIONAL_DATA_GATEWAY_TRANS_ID_CODE]??'';
            return $paymentId;
        }
        return '';
    }
}
