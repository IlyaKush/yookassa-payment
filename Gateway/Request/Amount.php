<?php
/**
 *  Amount
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    11.12.2021
 * Time:    22:19
 */
namespace IK\YooKassa\Gateway\Request;
use Magento\Payment\Gateway\Data\Order\OrderAdapter;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;

/**
 *
 */
class Amount extends AbstractRequest {

	/**
	 * @inheritDoc
	 */
	public function build(array $buildSubject) {
        if (!isset($buildSubject['amount'])
            || ($buildSubject['amount'] <= 0)
        ) {
            throw new \InvalidArgumentException('Wrong amount');
        }
        $amount  = $buildSubject['amount'];

        /** @var PaymentDataObjectInterface $paymentDO */
        $paymentDO = $buildSubject['payment'];
        /** @var OrderAdapter $order */
        $order   = $paymentDO->getOrder();

        return [
            'amount' => [
                'value'     => $amount,
                'currency'  => $order->getCurrencyCode(),
            ]
        ];
	}
}
