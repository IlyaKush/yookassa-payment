<?php
/**
 *  Initialization
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    10.12.2021
 * Time:    11:31
 */
namespace IK\YooKassa\Gateway\Request;
use IK\YooKassa\Gateway\Helper\VatCodeProcessor;
use IK\YooKassa\Helper\Data;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Payment\Gateway\Data\Order\OrderAdapter;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Model\Method\Logger;

/**
 *
 */
class Initialization extends AbstractRequest {

    const RETURNS_CONTROLLER_PARAM_CODE_ORDER = 'order';
    /**
     * @var VatCodeProcessor
     */
    protected $_vatCodeProcessor;

    /**
     * @param VatCodeProcessor    $vatCodeProcessor
     * @param SerializerInterface $serializer
     * @param Data                $helper
     * @param Logger              $logger
     */
    public function __construct(
        VatCodeProcessor $vatCodeProcessor,
        SerializerInterface $serializer,
        Data $helper,
        Logger $logger
    ) {
        parent::__construct($serializer, $helper, $logger);
        $this->_vatCodeProcessor = $vatCodeProcessor;
    }

    /**
	 * @inheritDoc
	 */
	public function build(array $buildSubject) {

        if (!isset($buildSubject['payment'])
            || !$buildSubject['payment'] instanceof PaymentDataObjectInterface
        ) {
            throw new \InvalidArgumentException('Payment data object should be provided');
        }

        /** @var PaymentDataObjectInterface $paymentDO */
        $paymentDO = $buildSubject['payment'];

        /** @var OrderAdapter $order */
        $order   = $paymentDO->getOrder();
        $payment = $paymentDO->getPayment();
        $paymentMethod = $payment->getMethod();
        $storeId = $order->getStoreId();
        $billingAddress = $order->getBillingAddress();
        /**
         * @see https://yookassa.ru/developers/api#create_payment
         */
        $paymentParameters = [
            'idempotence_key' => $this->_generateIdempotenceKey(),
            'metadata' => [
                'order_id' => $order->getOrderIncrementId(),
            ],
            'amount' => [
                'value'     => $order->getGrandTotalAmount(),
                'currency'  => $order->getCurrencyCode(),
            ],
            'payment_method_data' => [
                // @see https://yookassa.ru/developers/api#create_payment_payment_method_data
                'type'  => $this->_helper->getYooMethodOfGateway($storeId,$paymentMethod),
                'phone' => $this->_prepareTelephone($billingAddress->getTelephone())  // @see https://yookassa.ru/developers/api#create_payment_payment_method_data_mobile_balance_phone
            ],
            'confirmation' => [ // @see https://yookassa.ru/developers/api#create_payment_confirmation
                'type'       => 'redirect',
                'locale'     => $this->_helper->getLocale($storeId),
                'return_url' => $this->_helper->getReturnUrl([self::RETURNS_CONTROLLER_PARAM_CODE_ORDER => $order->getOrderIncrementId()]),
            ],
            'capture'     => (bool) $this->_helper->isAutoCaptureMode($storeId),
            'description' => __("Order %1 from site %2.",$order->getOrderIncrementId(),$this->_helper->getStoreUrl())->render(),
            'receipt'     => [
                'customer' => [
                    'full_name' => trim(sprintf("%s %s %s",  $billingAddress->getLastname(), $billingAddress->getFirstname(), $billingAddress->getMiddlename())),
                    'inn'       => $billingAddress->getVatId(), // https://yookassa.ru/developers/api#create_payment_receipt_customer_inn
                    'email'     => $billingAddress->getEmail(),
                    'phone'     => $this->_prepareTelephone($billingAddress->getTelephone()),
                ],
                'items' => []
            ]
        ];

        if($billingAddress->getCompany()){
            $paymentParameters['receipt']['customer']['full_name'] = $billingAddress->getCompany();
        }

        foreach ($order->getItems() as $item) {
            if (!$item->getParentItem()) {
                $paymentParameters['receipt']['items'][] = [
                    'quantity'     => (int)$item->getQtyOrdered(),
                    //'product_code' => '' <- need to convert to HEX // @see https://yookassa.ru/developers/api#create_payment_receipt_items_product_code
                    'description'  => sprintf("%s (%s)", $item->getName(), $item->getSku()),
                    'amount' => [
                        'value'    => $item->getPriceInclTax(),
                        'currency' => $order->getCurrencyCode(),
                    ],
                    'vat_code'        => $this->_vatCodeProcessor->getVatCode($item->getTaxPercent()), // @see https://yookassa.ru/developers/api#create_payment_receipt_items_vat_code
                    'payment_subject' => $this->_helper->getYooPaymentSubject($storeId,$paymentMethod),  // @see https://yookassa.ru/developers/api#create_payment_receipt_items_payment_subject
                    'payment_mode'    => $this->_helper->getYooPaymentMode($storeId,$paymentMethod), // @see https://yookassa.ru/developers/api#create_payment_receipt_items_payment_mode
                ];
            }
        }
        return $paymentParameters;
    }

    /**
     * @see https://ru.wikipedia.org/wiki/E.164
     * @param string $telephone
     *
     * @return string
     */
    protected function _prepareTelephone(string $telephone) {
        $telephone = str_replace('+','',$telephone);
        return $telephone;
    }
}
