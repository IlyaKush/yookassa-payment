<?php
/**
 *  PaymentId
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    11.12.2021
 * Time:    22:12
 */
namespace IK\YooKassa\Gateway\Request;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;

/**
 *
 */
class PaymentId extends AbstractRequest {
    public const TXN_ID_MASK = '%s' . PaymentId::TXN_ID_MASK_SEPARATOR . '%s';
    public const TXN_ID_MASK_SEPARATOR = '--';

    /**
	 * @inheritDoc
	 */
	public function build(array $buildSubject) {

        /** It is used in command fetch_transaction_info */
        if(isset($buildSubject['transactionId']) && $buildSubject['transactionId']){
            return [
                'id' => $this->_parseLastTransactionId($buildSubject['transactionId'])
            ];
        }

        if (!isset($buildSubject['payment'])
            || !$buildSubject['payment'] instanceof PaymentDataObjectInterface
        ) {
            throw new \InvalidArgumentException('Payment data object should be provided');
        }

        /** @var PaymentDataObjectInterface $paymentDO */
        $paymentDO = $buildSubject['payment'];

        $gatewayPaymentId = $this->_getGatewayPaymentId($paymentDO->getPayment())?:$this->_parseLastTransactionId($paymentDO->getPayment()->getLastTransId());

        return [
            'idempotence_key' => $this->_generateIdempotenceKey(),
            'id' => $gatewayPaymentId
        ];
	}

    /**
     * @param string $transactionId
     *
     * @return string
     */
    protected function _parseLastTransactionId($transactionId){
        $parts = explode(self::TXN_ID_MASK_SEPARATOR,$transactionId);
        return $parts[0];
    }
}
