<?php
/**
 *  TransactionType
 *
 * @copyright Copyright © 2022 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    21.01.2022
 * Time:    14:55
 */
namespace IK\YooKassa\Gateway\Request;
use IK\YooKassa\Helper\Data;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Payment\Model\Method\Logger;
use Magento\Sales\Api\Data\TransactionInterface;
use Magento\Sales\Api\TransactionRepositoryInterface;

/**
 *
 */
class TransactionType extends AbstractRequest {

    /**
     * @var TransactionRepositoryInterface
     */
    protected $transactionRepository;
    /**
     * @var SearchCriteriaBuilderFactory
     */
    protected $_searchCriteriaBuilderFactory;

    /**
     * @param TransactionRepositoryInterface $transactionRepository
     * @param SearchCriteriaBuilderFactory   $searchCriteriaBuilderFactory
     * @param SerializerInterface            $serializer
     * @param Data                           $helper
     * @param Logger                         $logger
     */
    public function __construct(
        TransactionRepositoryInterface $transactionRepository,
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory,
        SerializerInterface $serializer,
        Data $helper,
        Logger $logger) {
        parent::__construct($serializer, $helper, $logger);
        $this->_transactionRepository        = $transactionRepository;
        $this->_searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
    }

    /**
	 * @inheritDoc
	 */
	public function build(array $buildSubject) {

        /** It is used in command fetch_transaction_info */
        if(isset($buildSubject['transactionId']) && $buildSubject['transactionId']){

            $searchCriteriaBuilder = $this->_searchCriteriaBuilderFactory->create();
            $searchCriteriaBuilder->addFilter(
                TransactionInterface::TXN_ID,
                $buildSubject['transactionId']
            );
            $searchCriteria = $searchCriteriaBuilder
                ->setPageSize(1)
                ->setCurrentPage(1)
                ->create();

            $transactionList = $this->_transactionRepository->getList($searchCriteria);
            if (count($items = $transactionList->getItems())) {
                /** @var TransactionInterface $transaction */
                $transaction = current($items);
                $transaction->getTxnType();
                return [
                    'txn_type' => $transaction->getTxnType()
                ];
            }
            return [];
        }
	}
}
