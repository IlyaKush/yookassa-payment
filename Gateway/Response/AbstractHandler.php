<?php
/**
 *  AbstractHandler
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    10.12.2021
 * Time:    21:46
 */
namespace IK\YooKassa\Gateway\Response;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Gateway\Response\HandlerInterface;
use Magento\Sales\Model\Order\Payment as OrderPayment;
use YooKassa\Request\Payments\PaymentResponse;
use YooKassa\Request\Refunds\RefundResponse;

/**
 *
 */
abstract class AbstractHandler implements HandlerInterface {
    /**
     * @var SerializerInterface
     */
    protected $_serializer;

    /**
     * @param SerializerInterface $serializer
     */
    public function __construct(
        SerializerInterface $serializer
    ){
        $this->_serializer        = $serializer;
    }

	/**
	 * @inheritDoc
	 */
	public function handle(array $handlingSubject, array $response) {
        if (!isset($handlingSubject['payment'])
            || !$handlingSubject['payment'] instanceof PaymentDataObjectInterface
        ) {
            throw new \InvalidArgumentException('Payment data object should be provided');
        }


        try {
            $responsePayment = new PaymentResponse($response);
            return $this->_processResponsePayment($responsePayment, $handlingSubject);
        }
        catch (\Exception $e) {
            $responseRefund = new RefundResponse($response);
            return $this->_processResponseRefund($responseRefund,$handlingSubject);
        }

	}

    /**
     * @param OrderPayment $payment
     * @param string       $key
     * @param              $value
     */
    protected function _addPaymentAdditionalData(OrderPayment $payment, string $key, $value): void {
        $additional = $payment->getAdditionalData();
        if (!$additional) {
            $additional = [];
        } else {
            $additional = $this->_serializer->unserialize($additional);
        }
        if($value){
            $additional[$key] = $value;
        } else {
            unset($additional[$key]);
        }
        $payment->setAdditionalData($this->_serializer->serialize($additional));
    }

    /**
     * @param PaymentResponse $responsePayment
     * @param array           $handlingSubject
     *
     * @return void|array
     */
    abstract protected function _processResponsePayment(PaymentResponse $responsePayment, array $handlingSubject);

    /**
     * @param RefundResponse $responseRefund
     * @param array          $handlingSubject
     *
     * @return void|array
     */
    protected function _processResponseRefund(RefundResponse $responseRefund, array $handlingSubject){
        return;
    }
}
