<?php
/**
 *  TransactionAdditionalInfoHandler
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    12.12.2021
 * Time:    0:41
 */
namespace IK\YooKassa\Gateway\Response\Authorize;
use IK\YooKassa\Gateway\Response\AbstractHandler;
use IK\YooKassa\Gateway\Response\PaymentLinkHandler;
use Magento\Framework\Stdlib\DateTime as FrameworkDateTime;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Sales\Model\Order\Payment as OrderPayment;
use Magento\Sales\Model\Order\Payment\Transaction as PaymentTransactionModel;
use YooKassa\Request\Payments\PaymentResponse;

/**
 *
 */
class TransactionAdditionalInfoHandler extends AbstractHandler {

    /**
	 * @inheritDoc
	 */
	protected function _processResponsePayment(PaymentResponse $responsePayment, array $handlingSubject) {

        /** @var PaymentDataObjectInterface $paymentDO */
        $paymentDO = $handlingSubject['payment'];
        /** @var $payment OrderPayment */
        $payment = $paymentDO->getPayment();

        $payment->setTransactionId($responsePayment->getId());
        $payment->setIsTransactionClosed(false);

        /** set additional data */
        $rawDetails = (array)$payment->getAdditionalInformation();
        if($responsePayment->getAuthorizationDetails()){
            if($responsePayment->getAuthorizationDetails()->getRrn()){
                $rawDetails['RRN'] = $responsePayment->getAuthorizationDetails()->getRrn();
            }
            if($responsePayment->getAuthorizationDetails()->getAuthCode()){
                $rawDetails['Auth code'] = $responsePayment->getAuthorizationDetails()->getAuthCode();
            }
        }

        if($responsePayment->getExpiresAt()){
            $rawDetails['Expires at'] = $responsePayment->getExpiresAt()->format(FrameworkDateTime::DATETIME_PHP_FORMAT);
        }
        $payment->setTransactionAdditionalInfo(PaymentTransactionModel::RAW_DETAILS,$rawDetails);

        /** Unset payment link */
        $this->_addPaymentAdditionalData($payment,PaymentLinkHandler::PAYMENT_ADDITIONAL_DATA_REDIRECT_URL_CODE,null);
	}
}
