<?php
/**
 *  TransactionAdditionalInfoHandler
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    13.12.2021
 * Time:    21:52
 */
namespace IK\YooKassa\Gateway\Response\Cancel;
use IK\YooKassa\Gateway\Request\PaymentId;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Sales\Model\Order\Payment as OrderPayment;
use Magento\Sales\Model\Order\Payment\Transaction as PaymentTransactionModel;
use YooKassa\Request\Payments\PaymentResponse;

/**
 *
 */
class TransactionAdditionalInfoHandler extends \IK\YooKassa\Gateway\Response\Authorize\TransactionAdditionalInfoHandler {
    /**
     * @inheritDoc
     */
    protected function _processResponsePayment(PaymentResponse $responsePayment, array $handlingSubject) {

        parent::_processResponsePayment($responsePayment,$handlingSubject);

        /** @var PaymentDataObjectInterface $paymentDO */
        $paymentDO = $handlingSubject['payment'];
        /** @var $payment OrderPayment */
        $payment = $paymentDO->getPayment();

        $payment->setTransactionId( sprintf(PaymentId::TXN_ID_MASK,$responsePayment->getId(),'cancel'));
        $payment->setIsTransactionClosed(true);

        if($responsePayment->getCancellationDetails()){
            /** we get TransactionAdditionalInfo because some data was setted in parent class */
            $transactionAdditionalInfo = $payment->getTransactionAdditionalInfo();
            $rawDetails = $transactionAdditionalInfo[PaymentTransactionModel::RAW_DETAILS]??[];
            $rawDetails['Reason'] = $responsePayment->getCancellationDetails()->getReason();

            $payment->setTransactionAdditionalInfo(PaymentTransactionModel::RAW_DETAILS,$rawDetails);
        }

    }
}
