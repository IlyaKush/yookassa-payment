<?php
/**
 *  TransactionAdditionalInfoHandler
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    12.12.2021
 * Time:    22:21
 */
namespace IK\YooKassa\Gateway\Response\Capture;
use IK\YooKassa\Gateway\Request\PaymentId;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Sales\Model\Order\Payment as OrderPayment;
use YooKassa\Request\Payments\PaymentResponse;

/**
 *
 */
class TransactionAdditionalInfoHandler extends \IK\YooKassa\Gateway\Response\Authorize\TransactionAdditionalInfoHandler {

	/**
	 * @inheritDoc
	 */
	protected function _processResponsePayment(PaymentResponse $responsePayment, array $handlingSubject) {

        parent::_processResponsePayment($responsePayment,$handlingSubject);

        $amount = $handlingSubject['amount']??null;
        /** @var PaymentDataObjectInterface $paymentDO */
        $paymentDO = $handlingSubject['payment'];
        /** @var $payment OrderPayment */
        $payment = $paymentDO->getPayment();

        $payment->setTransactionId( sprintf(PaymentId::TXN_ID_MASK,$responsePayment->getId(),'capture'));
        if($payment->isCaptureFinal($amount)){
            $payment->setIsTransactionClosed(true);
        }
	}
}
