<?php
/**
 *  CreditCardData
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    11.12.2021
 * Time:    22:51
 */
namespace IK\YooKassa\Gateway\Response;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Sales\Model\Order\Payment as OrderPayment;
use YooKassa\Model\PaymentMethod\PaymentMethodBankCard;
use YooKassa\Request\Payments\PaymentResponse;

/**
 *
 */
class CreditCardData extends AbstractHandler {

	/**
	 * @inheritDoc
	 */
	protected function _processResponsePayment(PaymentResponse $responsePayment, array $handlingSubject) {
        /** @var PaymentDataObjectInterface $paymentDO */
        $paymentDO = $handlingSubject['payment'];
        /** @var $payment OrderPayment */
        $payment = $paymentDO->getPayment();

        if(($responsePaymentMethodCreditCard = $responsePayment->getPaymentMethod())
            && ($responsePaymentMethodCreditCard instanceof PaymentMethodBankCard)){
            /** @var PaymentMethodBankCard $responsePaymentMethodCreditCard */

            $payment->setCcType($responsePaymentMethodCreditCard->getCardType());
            $payment->setCcLast4($responsePaymentMethodCreditCard->getLast4());
            $payment->setCcExpMonth($responsePaymentMethodCreditCard->getExpiryMonth());
            $payment->setCcExpYear($responsePaymentMethodCreditCard->getExpiryYear());
            $payment->setCcTransId($responsePayment->getId());


            $payment->setAdditionalInformation('Name',$responsePaymentMethodCreditCard->getTitle());
            $payment->setAdditionalInformation('Type',$payment->getCcType());
            $payment->setAdditionalInformation('Number',sprintf('%s... %s',$responsePaymentMethodCreditCard->getFirst6(),$responsePaymentMethodCreditCard->getLast4()));
            if($responsePaymentMethodCreditCard->getIssuerCountry()){
                $payment->setAdditionalInformation('Issuer Country',$responsePaymentMethodCreditCard->getIssuerCountry());
            }
            if($responsePaymentMethodCreditCard->getIssuerName()){
                $payment->setAdditionalInformation('Issuer Name',$responsePaymentMethodCreditCard->getIssuerName());
            }
            $payment->setAdditionalInformation('Expiration Date',sprintf('%s/%s',$responsePaymentMethodCreditCard->getExpiryMonth(),$responsePaymentMethodCreditCard->getExpiryYear()));
        }
	}
}
