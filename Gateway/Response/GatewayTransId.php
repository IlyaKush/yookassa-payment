<?php
/**
 *  GatewayTransId
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    10.12.2021
 * Time:    22:01
 */
namespace IK\YooKassa\Gateway\Response;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Sales\Model\Order\Payment as OrderPayment;
use YooKassa\Request\Payments\PaymentResponse;

/**
 *
 */
class GatewayTransId extends AbstractHandler {

    const PAYMENT_ADDITIONAL_DATA_GATEWAY_TRANS_ID_CODE = 'gateway_trans_id';

	/**
	 * @inheritDoc
	 */
	protected function _processResponsePayment(PaymentResponse $responsePayment, array $handlingSubject) {

        /** @var PaymentDataObjectInterface $paymentDO */
        $paymentDO = $handlingSubject['payment'];
        /** @var $payment OrderPayment */
        $payment = $paymentDO->getPayment();

        //$payment->setLastTransId($responsePayment->getId());
        //$payment->setAdditionalInformation('Gateway trans id', $responsePayment->getId());
        $this->_addPaymentAdditionalData($payment,self::PAYMENT_ADDITIONAL_DATA_GATEWAY_TRANS_ID_CODE,$responsePayment->getId());
	}
}
