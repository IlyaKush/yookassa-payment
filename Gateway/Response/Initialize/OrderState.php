<?php
/**
 *  OrderState
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    10.12.2021
 * Time:    22:05
 */
namespace IK\YooKassa\Gateway\Response\Initialize;
use IK\YooKassa\Gateway\Response\AbstractHandler;
use Magento\Framework\DataObject;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Config as OrderConfig;
use YooKassa\Request\Payments\PaymentResponse;

/**
 *
 */
class OrderState extends AbstractHandler {

    const INITIALIZED_PAYMENT_ORDER_STATE_VALUE = Order::STATE_PENDING_PAYMENT;
    /**
     * @var OrderConfig
     */
    protected $_orderConfig;

    /**
     * @param OrderConfig         $orderConfig
     * @param SerializerInterface $serializer
     */
    public function __construct(
        OrderConfig         $orderConfig,
        SerializerInterface $serializer
    ) {
        parent::__construct($serializer);
        $this->_orderConfig = $orderConfig;
    }

    /**
	 * @inheritDoc
	 */
	protected function _processResponsePayment(PaymentResponse $responsePayment, array $handlingSubject) {
        if (!isset($handlingSubject['stateObject'])
            || !$handlingSubject['stateObject'] instanceof DataObject
        ) {
            throw new \InvalidArgumentException('State object should be provided');
        }
        $stateObject = $handlingSubject['stateObject'];
        $stateObject->setData('state',self::INITIALIZED_PAYMENT_ORDER_STATE_VALUE);
        $stateObject->setData('status',$this->_orderConfig->getStateDefaultStatus(self::INITIALIZED_PAYMENT_ORDER_STATE_VALUE));
        $stateObject->setData('is_notified',1);
	}
}
