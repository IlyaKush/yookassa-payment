<?php
/**
 *  PaymentLinkHandler
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    10.12.2021
 * Time:    21:56
 */
namespace IK\YooKassa\Gateway\Response;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Sales\Model\Order\Payment as OrderPayment;
use YooKassa\Request\Payments\PaymentResponse;

/**
 *
 */
class PaymentLinkHandler extends AbstractHandler {

    const PAYMENT_ADDITIONAL_DATA_REDIRECT_URL_CODE = 'payment_link';

	/**
	 * @inheritDoc
	 */
	protected function _processResponsePayment(PaymentResponse $responsePayment, array $handlingSubject) {
        /** @var PaymentDataObjectInterface $paymentDO */
        $paymentDO = $handlingSubject['payment'];
        /** @var $payment OrderPayment */
        $payment = $paymentDO->getPayment();
        $this->_addPaymentAdditionalData($payment,self::PAYMENT_ADDITIONAL_DATA_REDIRECT_URL_CODE,$responsePayment->getConfirmation()->getConfirmationUrl());
	}
}
