<?php
/**
 *  TestDataHandler
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    12.12.2021
 * Time:    13:52
 */
namespace IK\YooKassa\Gateway\Response;
use IK\YooKassa\Helper\Data;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Sales\Model\Order\Payment as OrderPayment;
use YooKassa\Request\Payments\PaymentResponse;
use YooKassa\Request\Refunds\RefundResponse;

/**
 *
 */
class TestDataHandler extends AbstractHandler {
    /**
     * @var Data
     */
    protected $_helper;

    /**
     * @param SerializerInterface $serializer
     * @param Data                $helper
     */
    public function __construct(SerializerInterface $serializer, Data $helper) {
        parent::__construct($serializer);
        $this->_helper = $helper;
    }

    /**
	 * @inheritDoc
	 */
	protected function _processResponsePayment(PaymentResponse $responsePayment, array $handlingSubject) {
		if($responsePayment->getTest()){
            /** @var PaymentDataObjectInterface $paymentDO */
            $paymentDO = $handlingSubject['payment'];
            /** @var $payment OrderPayment */
            $payment = $paymentDO->getPayment();

            $payment->setAdditionalInformation('Mode', 'Sandbox transaction!');

            $order = $payment->getOrder();
            $storeId = $order->getStoreId();
            if(!$this->_helper->isTestMode($storeId)){
                $payment->setIsTransactionPending(true);
                $payment->setIsFraudDetected(true);
                $order->addCommentToStatusHistory(__('Order attempted paid with test card!!!!'));
            } else {
                if($order->isPaymentReview()){
                    $payment->setIsTransactionPending(false);
                    $payment->setIsTransactionApproved(true);
                    if($order->isFraudDetected()){
                        $payment->setIsFraudDetected(false);
                    }
                    $order->addCommentToStatusHistory(__('Now is test mode. Test card was accepted.'));
                }
            }
        }
	}

    /**
     * @inheritDoc
     */
    protected function _processResponseRefund(RefundResponse $responseRefund, array $handlingSubject) {
        /** We don't need to apply TestDataHandler on RefundResponse */
    }
}
