<?php
/**
 *  UpdateTransactionAdditionalInfoHandler
 *
 * @copyright Copyright © 2022 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    22.01.2022
 * Time:    15:17
 */
namespace IK\YooKassa\Gateway\Response;
use Magento\Framework\Stdlib\DateTime as FrameworkDateTime;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Sales\Model\Order\Payment as OrderPayment;
use Magento\Sales\Model\Order\Payment\Transaction as PaymentTransactionModel;
use YooKassa\Request\Payments\PaymentResponse;
use YooKassa\Request\Refunds\RefundResponse;

/**
 *
 */
class UpdateTransactionAdditionalInfoHandler extends AbstractHandler {

	/**
	 * @inheritDoc
	 */
	protected function _processResponsePayment(PaymentResponse $responsePayment, array $handlingSubject) {
        /** @var PaymentDataObjectInterface $paymentDO */
        $paymentDO = $handlingSubject['payment'];
        /** @var $payment OrderPayment */
        $payment = $paymentDO->getPayment();

        /** set additional data */
        $rawDetails = (array)$payment->getAdditionalInformation();
        $rawDetails['RRN'] = $responsePayment->getAuthorizationDetails()->getRrn();
        $rawDetails['Auth code'] = $responsePayment->getAuthorizationDetails()->getAuthCode();
        if($responsePayment->getExpiresAt()){
            $rawDetails['Expires at'] = $responsePayment->getExpiresAt()->format(FrameworkDateTime::DATETIME_PHP_FORMAT);
        }
        $payment->setTransactionAdditionalInfo(PaymentTransactionModel::RAW_DETAILS,$rawDetails);
        return $payment->getTransactionAdditionalInfo()[PaymentTransactionModel::RAW_DETAILS]??void;
	}

    /**
     * @inheritDoc
     */
    protected function _processResponseRefund(RefundResponse $responseRefund, array $handlingSubject) {
        /** @var PaymentDataObjectInterface $paymentDO */
        $paymentDO = $handlingSubject['payment'];
        /** @var $payment OrderPayment */
        $payment = $paymentDO->getPayment();

        /** set additional data */
        $rawDetails = (array)$payment->getAdditionalInformation();
        $rawDetails['Payment Id'] = $responseRefund->getPaymentId();

        $payment->setTransactionAdditionalInfo(PaymentTransactionModel::RAW_DETAILS,$rawDetails);
        return $payment->getTransactionAdditionalInfo()[PaymentTransactionModel::RAW_DETAILS]??void;
    }
}
