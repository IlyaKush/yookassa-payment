<?php
/**
 *  YooMoneyData
 *
 * @copyright Copyright © 2022 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    23.01.2022
 * Time:    19:17
 */
namespace IK\YooKassa\Gateway\Response;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Sales\Model\Order\Payment as OrderPayment;
use YooKassa\Model\PaymentMethod\PaymentMethodYooMoney;
use YooKassa\Request\Payments\PaymentResponse;
/**
 *
 */
class YooMoneyData extends AbstractHandler {

	/**
	 * @inheritDoc
	 */
	protected function _processResponsePayment(PaymentResponse $responsePayment, array $handlingSubject) {
        /** @var PaymentDataObjectInterface $paymentDO */
        $paymentDO = $handlingSubject['payment'];
        /** @var $payment OrderPayment */
        $payment = $paymentDO->getPayment();

        if(($responsePaymentMethodYooMoney = $responsePayment->getPaymentMethod())
            && ($responsePaymentMethodYooMoney instanceof PaymentMethodYooMoney)) {
            $payment->setAdditionalInformation('Name',$responsePaymentMethodYooMoney->getTitle());
            $payment->setAdditionalInformation('Number',$responsePaymentMethodYooMoney->getAccountNumber());
        }
    }
}
