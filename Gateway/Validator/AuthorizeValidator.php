<?php
/**
 *  AuthorizeValidator
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    12.12.2021
 * Time:    17:53
 */
namespace IK\YooKassa\Gateway\Validator;
use YooKassa\Model\PaymentStatus;
use YooKassa\Request\Payments\PaymentResponse;

/**
 *
 */
class AuthorizeValidator extends ResponseObjectValidator {
    /**
     * @inheritDoc
     */
    protected function _validateResponseObject(PaymentResponse $responsePayment) {
        $errors = [];

        if($responsePayment->getStatus() != PaymentStatus::WAITING_FOR_CAPTURE){
            $errors[] = __("Expected status '%1'. '%2' received.",PaymentStatus::WAITING_FOR_CAPTURE,$responsePayment->getStatus());
        }

        if(!$responsePayment->getPaid()){
            $errors[] = __('Payment is not paid.');
        }

        if (empty($errors)) {
            return $this->createResult(
                true,
                []
            );
        } else {
            return $this->createResult(
                false,
                $errors
            );
        }
    }
}
