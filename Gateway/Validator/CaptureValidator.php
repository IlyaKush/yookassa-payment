<?php
/**
 *  CaptureValidator
 *
 * @copyright Copyright © 2022 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    17.01.2022
 * Time:    18:21
 */
namespace IK\YooKassa\Gateway\Validator;
use YooKassa\Model\PaymentStatus;
use YooKassa\Request\Payments\PaymentResponse;

/**
 *
 */
class CaptureValidator extends ResponseObjectValidator {

	/**
	 * @inheritDoc
	 */
	protected function _validateResponseObject(PaymentResponse $responsePayment) {
        $errors = [];

        if($responsePayment->getStatus() != PaymentStatus::SUCCEEDED){
            $errors[] = __("Expected status '%1'. '%2' received.",PaymentStatus::SUCCEEDED,$responsePayment->getStatus());
        }

        if(!$responsePayment->getPaid()){
            $errors[] = __('Payment is not paid.');
        }

        if (empty($errors)) {
            return $this->createResult(
                true,
                []
            );
        } else {
            return $this->createResult(
                false,
                $errors
            );
        }
	}
}
