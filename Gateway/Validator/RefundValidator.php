<?php
/**
 *  RefundValidator
 *
 * @copyright Copyright © 2022 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    17.01.2022
 * Time:    18:29
 */
namespace IK\YooKassa\Gateway\Validator;
use Magento\Payment\Gateway\Validator\AbstractValidator;
use Magento\Payment\Gateway\Validator\ResultInterface;
use YooKassa\Model\RefundStatus;
use YooKassa\Request\Refunds\RefundResponse;
/**
 *
 */
class RefundValidator extends AbstractValidator {

    /**
     * @inheritDoc
     */
    public function validate(array $validationSubject) {
        if (!isset($validationSubject['response']) || !is_array($validationSubject['response'])) {
            throw new \InvalidArgumentException('Response does not exist');
        }
        try {
            $responseRefund = new RefundResponse($validationSubject['response']);
            $errors = [];

            if($responseRefund->getStatus() != RefundStatus::SUCCEEDED){
                $errors[] = __("Expected status '%1'. '%2' received.",RefundStatus::SUCCEEDED,$responseRefund->getStatus());
            }

            if (empty($errors)) {
                return $this->createResult(
                    true,
                    []
                );
            } else {
                return $this->createResult(
                    false,
                    $errors
                );
            }
        }
        catch (\Exception $e) {
            return $this->createResult(
                false,
                [$e->getMessage()]
            );
        }

    }
}
