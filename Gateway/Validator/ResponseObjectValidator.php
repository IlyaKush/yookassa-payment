<?php
/**
 *  ResponseObjectValidator
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    12.12.2021
 * Time:    17:44
 */
namespace IK\YooKassa\Gateway\Validator;
use Magento\Payment\Gateway\Validator\AbstractValidator;
use Magento\Payment\Gateway\Validator\ResultInterface;
use YooKassa\Request\Payments\PaymentResponse;

/**
 *
 */
abstract class ResponseObjectValidator extends AbstractValidator {
	/**
	 * @inheritDoc
	 */
	public function validate(array $validationSubject) {
        if (!isset($validationSubject['response']) || !is_array($validationSubject['response'])) {
            throw new \InvalidArgumentException('Response does not exist');
        }

        try {
            $responsePayment = new PaymentResponse($validationSubject['response']);
            return $this->_validateResponseObject($responsePayment);
        }
        catch (\Exception $e) {
            return $this->createResult(
                false,
                [$e->getMessage()]
            );
        }
    }

    /**
     * @param PaymentResponse $responsePayment
     *
     * @return ResultInterface
     */
    abstract protected function _validateResponseObject(PaymentResponse $responsePayment);
}
