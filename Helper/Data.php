<?php
/**
 *  Data
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    07.12.2021
 * Time:    13:17
 */
namespace IK\YooKassa\Helper;
use IK\YooKassa\Gateway\Config\General;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface as StoreScopeInterface;

/**
 *
 */
class Data  extends AbstractHelper {

    const CREDIT_CARD_METHOD_CODE = 'yookassa';
    const YOO_MONEY_METHOD_CODE   = 'yoomoney';

    const GENERAL_SETTINGS_CODE           = self::CREDIT_CARD_METHOD_CODE;
    const GENERAL_CONFIG_XML_PATH         = 'payment/%s/%s';
    const SHOP_ID_XML_CODE                = 'shop_id';
    const PRIVATE_KEY_XML_CODE            = 'private_key';
    const TESTMODE_XML_CODE               = 'testmode';
    const AUTOCAPTURE_XML_CODE            = 'autocapture';
    const SEND_ORDER_EMAIL_XML_CODE       = 'send_order_email';
    const SEND_INVOICE_EMAIL_XML_CODE     = 'send_invoice_email';
    const METHOD_LOGO_XML_CODE             = 'logos';
    const CANCEL_TIMEOUT_XML_CODE         = 'timeout_to_cancel';
    const YOO_PAYMENT_METHOD_XML_CODE     = 'yoo_payment_method';
    const YOO_PAYMENT_MODE_XML_CODE       = 'yoo_payment_mode';
    const YOO_PAYMENT_SUBJECT_XML_CODE    = 'yoo_payment_subject';

    const REDIRECT_CONTROLLER_PATH        = 'yookassa/payment/redirect';
    const RETURNS_CONTROLLER_PATH         = 'yookassa/payment/returns';
    const CALLBACK_CONTROLLER_PATH        = 'yookassa/payment/callback';


	/**
     * @param null|int|string $storeId
     *
     * @return string
     */
    public function getShopId($storeId = null) {
        return $this->scopeConfig->getValue(sprintf(self::GENERAL_CONFIG_XML_PATH,self::GENERAL_SETTINGS_CODE,self::SHOP_ID_XML_CODE), StoreScopeInterface::SCOPE_STORE,$storeId);
    }

    /**
     * @param null|int|string $storeId
     *
     * @return string
     */
    public function getPrivateKey($storeId = null) {
        return $this->scopeConfig->getValue(sprintf(self::GENERAL_CONFIG_XML_PATH,self::GENERAL_SETTINGS_CODE,self::PRIVATE_KEY_XML_CODE), StoreScopeInterface::SCOPE_STORE,$storeId);
    }

    /**
     * @param array $params
     *
     * @return string
     */
    public function getRedirectUrl($params = []){
        return $this->_getUrl(self::REDIRECT_CONTROLLER_PATH,$params);
    }

    /**
     * @param array $params
     *
     * @return string
     */
    public function getReturnUrl($params = []){
        return $this->_getUrl(self::RETURNS_CONTROLLER_PATH,$params);
    }

    /**
     * @param null|int|string $storeId
     *
     * @return bool
     */
    public function isTestMode($storeId = null){
        return $this->scopeConfig->isSetFlag(sprintf(self::GENERAL_CONFIG_XML_PATH,self::GENERAL_SETTINGS_CODE,self::TESTMODE_XML_CODE), StoreScopeInterface::SCOPE_STORE,$storeId);
    }

    /**
     * @param null|int|string $storeId
     *
     * @return bool
     */
    public function isAutoCaptureMode($storeId = null){
        return $this->scopeConfig->isSetFlag(sprintf(self::GENERAL_CONFIG_XML_PATH,self::GENERAL_SETTINGS_CODE,self::AUTOCAPTURE_XML_CODE), StoreScopeInterface::SCOPE_STORE,$storeId);
    }

    /**
     * @param null|int|string $storeId
     *
     * @return bool
     */
    public function ifSendOrderConformationEmailByDefaultMagentoLogic($storeId = null){
        return $this->scopeConfig->isSetFlag(sprintf(self::GENERAL_CONFIG_XML_PATH,self::GENERAL_SETTINGS_CODE,self::SEND_ORDER_EMAIL_XML_CODE), StoreScopeInterface::SCOPE_STORE,$storeId);
    }

    /**
     * @param null|int|string $storeId
     *
     * @return bool
     */
    public function ifSendInvoiceEmail($storeId = null){
        return $this->scopeConfig->isSetFlag(sprintf(self::GENERAL_CONFIG_XML_PATH,self::GENERAL_SETTINGS_CODE,self::SEND_INVOICE_EMAIL_XML_CODE), StoreScopeInterface::SCOPE_STORE,$storeId);
    }

    /**
     * @param null|int|string $storeId
     * @param string          $methodCode
     *
     * @return string
     */
    public function getMethodLogo($storeId = null, $methodCode = self::GENERAL_SETTINGS_CODE){
        return $this->_getValue(self::METHOD_LOGO_XML_CODE,$methodCode,$storeId);
    }

    /**
     * @param null|int|string $storeId
     *
     * @return int
     */
    public function getCancelTimeout($storeId = null){
        return (int) $this->scopeConfig->getValue(sprintf(self::GENERAL_CONFIG_XML_PATH,self::GENERAL_SETTINGS_CODE,self::CANCEL_TIMEOUT_XML_CODE), StoreScopeInterface::SCOPE_STORE,$storeId);
    }

    /**
     * @return string
     */
    public function getStoreUrl(){
        return $this->_getUrl('');
    }

    /**
     * @param null|int|string $storeId
     * @return bool
     */
    public function isOneStepCheckout($storeId = null): bool {
        if($this->scopeConfig->isSetFlag('amasty_checkout/general/enabled',StoreScopeInterface::SCOPE_STORE,$storeId)){
            return true;
        }
        return false;
    }

    /**
     * Get a store locale
     *
     * @param null|int|string $storeId
     *
     * @return string
     */
    public function getLocale($storeId = null) {
        /**
         * https://yookassa.ru/developers/api#create_payment_confirmation_redirect_locale
         */
        $locale = $this->scopeConfig->getValue('general/locale/code',StoreScopeInterface::SCOPE_STORE, $storeId);
        if(in_array($locale,['ru_RU','be_BY','uk_UA','lt_LT','lv_LV','et_EE','ka_GE','kz_KZ','kk_KZ','az_Latn_AZ','az_AZ','hy_AM','tt_RU','tg_TJ'])){
            return 'ru_RU';
        }
        return 'en_US';
    }

    /**
     * @param string $field
     *
     * @return bool
     */
    protected function _isCommonField($field){
        return in_array($field,General::IN_COMMON_FIELDS);
    }

    /**
     * @param string          $field
     * @param string          $methodCode
     * @param null|int|string $storeId
     *
     * @return bool
     */
    protected function _isSetFlag($field, $methodCode = self::GENERAL_SETTINGS_CODE, $storeId = null) {
        if($this->_isCommonField($field)){
            return $this->scopeConfig->isSetFlag(sprintf(self::GENERAL_CONFIG_XML_PATH,self::GENERAL_SETTINGS_CODE,$field), StoreScopeInterface::SCOPE_STORE,$storeId);
        } else {
            return $this->scopeConfig->isSetFlag(sprintf(self::GENERAL_CONFIG_XML_PATH,$methodCode,$field), StoreScopeInterface::SCOPE_STORE,$storeId);
        }
    }
    /**
     * @param string          $field
     * @param string          $methodCode
     * @param null|int|string $storeId
     *
     * @return mixed
     */
    protected function _getValue($field, $methodCode = self::GENERAL_SETTINGS_CODE, $storeId = null){
        if($this->_isCommonField($field)){
            return $this->scopeConfig->getValue(sprintf(self::GENERAL_CONFIG_XML_PATH,self::GENERAL_SETTINGS_CODE,$field), StoreScopeInterface::SCOPE_STORE,$storeId);
        } else {
            return $this->scopeConfig->getValue(sprintf(self::GENERAL_CONFIG_XML_PATH,$methodCode,$field), StoreScopeInterface::SCOPE_STORE,$storeId);
        }
    }

    /**
     * Get payment method
     *
     * @see https://yookassa.ru/developers/api#create_payment_payment_method_data
     *
     * @param null|int|string $storeId
     * @param string          $methodCode
     *
     * @return string
     */
    public function getYooMethodOfGateway($storeId = null, $methodCode = self::GENERAL_SETTINGS_CODE) {
        return $this->_getValue(self::YOO_PAYMENT_METHOD_XML_CODE,$methodCode,$storeId);
    }

    /**
     * Get payment subject
     *
     * @see https://yookassa.ru/developers/54fz/parameters-values#payment-subject
     *
     * @param null|int|string $storeId
     * @param string          $methodCode
     *
     * @return string
     */
    public function getYooPaymentSubject($storeId = null, $methodCode = self::GENERAL_SETTINGS_CODE) {
        return $this->_getValue(self::YOO_PAYMENT_SUBJECT_XML_CODE, $methodCode,$storeId);
    }

    /**
     * Get payment mode
     *
     * @see https://yookassa.ru/developers/54fz/parameters-values#payment-mode
     *
     * @param null|int|string $storeId
     * @param string          $methodCode
     *
     * @return string
     */
    public function getYooPaymentMode($storeId = null, $methodCode = self::GENERAL_SETTINGS_CODE) {
        return $this->_getValue(self::YOO_PAYMENT_MODE_XML_CODE,$methodCode,$storeId);
    }
}
