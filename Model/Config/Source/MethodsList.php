<?php
/**
 *  MethodsList
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    07.12.2021
 * Time:    16:07
 */
namespace IK\YooKassa\Model\Config\Source;
use IK\YooKassa\Model\Payment\Method\Specification\Group as GroupSpecification;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\Payment\Helper\Data as PaymentHelperData;

/**
 *
 */
class MethodsList implements OptionSourceInterface {

    /** @var  PaymentHelperData */
    protected $paymentHelper;

    /**
     * @var GroupSpecification
     */
    protected $gatewaySpecification;

    /**
     * @param PaymentHelperData  $paymentHelper
     * @param GroupSpecification $gatewaySpecification
     */
    public function __construct(
        PaymentHelperData  $paymentHelper,
        GroupSpecification $gatewaySpecification
    ) {
        $this->paymentHelper          = $paymentHelper;
        $this->gatewaySpecification = $gatewaySpecification;
    }

	/**
	 * @inheritDoc
	 */
	public function toOptionArray() {
        $result = [];
        foreach ($this->paymentHelper->getPaymentMethods() as $code => $data) {
            if ($this->gatewaySpecification->isSatisfiedBy($code)) {
                $result[] = [
                    'label' => $data['title'] ?? $this->paymentHelper->getMethodInstance($code)->getTitle(),
                    'value' => $code
                ];
            }
        }
        return $result;
	}
}
