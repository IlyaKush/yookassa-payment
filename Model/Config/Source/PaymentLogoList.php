<?php
/**
 *  PaymentLogoList
 *
 * @copyright Copyright © 2022 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    24.01.2022
 * Time:    15:30
 */
namespace IK\YooKassa\Model\Config\Source;
use Magento\Framework\Data\OptionSourceInterface;
/**
 *
 */
class PaymentLogoList implements OptionSourceInterface {

	/**
	 * @inheritDoc
	 */
	public function toOptionArray() {
        $result = [];
        foreach ($this->getValuesArray() as $value => $label ){
            $result[$value] = ['label' => $label, 'value' => $value];
        }
        return $result;
	}

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function getValuesArray() {
        return [
            'visa.svg'       => __('VISA'),
            'mastercard.svg' => __('MasterCard'),
            'maestro.svg'    => __('Maestro'),
            'jcb.svg'        => __('JCB'),
            'mir.svg'        => __('Mir')
        ];
    }
}
