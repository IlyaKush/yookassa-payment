<?php
/**
 *  Group
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    07.12.2021
 * Time:    16:03
 */
namespace IK\YooKassa\Model\Payment\Method\Specification;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Payment\Model\Method\SpecificationInterface;
/**
 *
 */
class Group implements SpecificationInterface {

    CONST YOOKASSA_GROUP_CODE = 'yookassa_group';

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @inheritDoc
     */
    public function isSatisfiedBy($paymentMethod) {
        foreach ($this->_scopeConfig->getValue('payment') as $code => $data) {
            if($paymentMethod == $code){
                if (isset($data['group']) && $data['group'] == self::YOOKASSA_GROUP_CODE) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return array
     */
    public function getGroupMethods(){
        $methods = [];
        foreach ($this->_scopeConfig->getValue('payment') as $code => $data) {
            if (isset($data['group']) && $data['group'] == self::YOOKASSA_GROUP_CODE) {
                $methods[] = $code;
            }
        }

        return $methods;
    }
}
