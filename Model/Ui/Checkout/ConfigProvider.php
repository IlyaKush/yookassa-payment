<?php
/**
 *  ConfigProvider
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    07.12.2021
 * Time:    15:42
 */
namespace IK\YooKassa\Model\Ui\Checkout;
use IK\YooKassa\Helper\Data;
use IK\YooKassa\Model\Config\Source\MethodsList;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Checkout\Model\Session;
use Magento\Framework\View\Asset\Repository as AssetRepository;

/**
 *
 */
class ConfigProvider implements ConfigProviderInterface {

    /** uncomment if filenames in config without type */
    //CONST LOGO_FILE_PNG_PATTERN = "%s.svg";
    CONST LOGO_FILE_PNG_PATTERN = "%s";

    /**
     * @var AssetRepository
     */
    protected $_assetRepo;
    /**
     * @var MethodsList
     */
    protected $_methodsList;
    /**
     * @var Data
     */
    protected $_helper;
    /**
     * @var Session
     */
    protected $_session;

    /**
     * @param Session         $session
     * @param Data            $helper
     * @param MethodsList     $methodsList
     * @param AssetRepository $assetRepo
     */
    public function __construct(
        Session                                  $session,
        Data                                     $helper,
        MethodsList                              $methodsList,
        AssetRepository                          $assetRepo
    ){
        $this->_assetRepo   = $assetRepo;
        $this->_methodsList = $methodsList;
        $this->_helper = $helper;
        $this->_session = $session;
    }

	/**
	 * @inheritDoc
	 */
	public function getConfig() {
        $configArray = [];
        $storeId = $this->_session->getQuote()->getStoreId();
        foreach ($this->_methodsList->toOptionArray() as $_method){
            $configArray['payment'][$_method['value']] = [
                'redirect_url' => $this->_helper->getRedirectUrl(),
                'logo' => $this->getMethodLogo($_method['value'],$storeId),
            ];
        }
        return $configArray;
	}

    /**
     * @param string $methodCode
     * @param null|int|string $storeId
     * @return string[]
     */
    public function getMethodLogo(string $methodCode,$storeId = null) {
        $nameFileLogo = [];
        $gatewayLogos = $this->_helper->getMethodLogo($storeId,$methodCode);
        $gatewayLogos = explode(',',$gatewayLogos);
        foreach ($gatewayLogos as $key => $_logoFile){
            $nameFileLogo[$key] = $_logoFile?sprintf(self::LOGO_FILE_PNG_PATTERN, $_logoFile):null;
        }

        $logos = [];
        if($nameFileLogo && !empty($nameFileLogo)){
            foreach ($nameFileLogo as $_logoFileName) {
                if($_logoFileName && !empty($_logoFileName)){
                    $logos[] = $this->_assetRepo->getUrl(sprintf("IK_YooKassa::images/logo/%s", $_logoFileName));
                }
            }
        }
        return $logos;
    }

}
