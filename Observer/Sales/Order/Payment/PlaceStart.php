<?php
/**
 *  PlaceStart
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    07.12.2021
 * Time:    19:09
 */
namespace IK\YooKassa\Observer\Sales\Order\Payment;
use IK\YooKassa\Helper\Data;
use IK\YooKassa\Model\Payment\Method\Specification\Group;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment;
/**
 *
 */
class PlaceStart implements ObserverInterface {
    /**
     * @var Group
     */
    protected $_specificationGroup;
    /**
     * @var Data
     */
    protected $_helper;

    /**
     * @param Data  $helper
     * @param Group $specificationGroup
     */
    public function __construct(
        Data  $helper,
        Group $specificationGroup
    ) {
        $this->_specificationGroup = $specificationGroup;
        $this->_helper             = $helper;
    }

	/**
	 * @inheritDoc
	 */
	public function execute(Observer $observer) {
        /** @var Payment $payment */
        $payment = $observer->getPayment();

        if($this->_specificationGroup->isSatisfiedBy($payment->getMethod())){
            $order = $payment->getOrder();
            $emailSend = $this->_helper->ifSendOrderConformationEmailByDefaultMagentoLogic($order->getStoreId());
            /** @var Order $order */
            $order->setCanSendNewEmailFlag($emailSend)
                ->setIsCustomerNotified($emailSend);
        }
	}
}
