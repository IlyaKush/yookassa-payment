<?php
/**
 *  OrderCancelAfter
 *
 * @copyright Copyright © 2022 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 * Date:    17.01.2022
 * Time:    20:09
 */
namespace IK\YooKassa\Observer\Sales;
use IK\YooKassa\Gateway\Response\PaymentLinkHandler;
use IK\YooKassa\Model\Payment\Method\Specification\Group;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Sales\Model\Order;
/**
 *
 */
class OrderCancelAfter implements ObserverInterface {
    /**
     * @var SerializerInterface
     */
    protected $_serializer;
    /**
     * @var Group
     */
    protected $_specification;

    /**
     * @param Group               $specification
     * @param SerializerInterface $serializer
     */
    public function __construct(
        Group $specification,
        SerializerInterface $serializer
    ) {
        $this->_serializer    = $serializer;
        $this->_specification = $specification;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        /** @var Order $order */
        $order = $observer->getOrder();
        $payment = $order->getPayment();

        if($this->_specification->isSatisfiedBy($payment->getMethod())){
            $additional = $payment->getAdditionalData();
            if ($additional) {
                $additional = $this->_serializer->unserialize($additional);
                unset($additional[PaymentLinkHandler::PAYMENT_ADDITIONAL_DATA_REDIRECT_URL_CODE]);
            }
            $payment->setAdditionalData($this->_serializer->serialize($additional));
        }
    }
}
