# README #
По вопросам установки обращаться на почту [ilya.a.kush@yandex.ru](mailto:ilya.a.kush@yandex.ru)
с пометкой ***модуль IK_YooKassa***
### Установка ###

* Установить [yookassa-sdk-php](https://github.com/yoomoney/yookassa-sdk-php) через composer
```
composer require yoomoney/yookassa-sdk-php
```
* Скопировать файлы модуля в папку app\code\IK\YooKassa
* Выполнить команды 
```
php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy
php bin/magento cache:clean
```
----------------------------------------------------------------
### Installation ###

* Install [yookassa-sdk-php](https://github.com/yoomoney/yookassa-sdk-php) by composer
```
composer require yoomoney/yookassa-sdk-php
```
* Copy source files in folder app\code\IK\YooKassa
* Run commands
```
php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy
php bin/magento cache:clean
```
