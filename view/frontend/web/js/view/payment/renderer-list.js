/**
 *  renderer-list
 *
 * @copyright Copyright © 2021 https://headwayit.com/ HeadWayIt. All rights reserved.
 * @author    Ilya Kushnir ilya.kush@gmail.com
 */
define([
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ], function (
        Component,
        rendererList
    ){
        'use strict';
        rendererList.push(
            {
                type: 'yookassa',
                component: 'IK_YooKassa/js/view/payment/method-renderer/default'
            },
            {
                type: 'yoomoney',
                component: 'IK_YooKassa/js/view/payment/method-renderer/default'
            },
        );
        return Component.extend({});
    }
);
